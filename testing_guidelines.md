# Testing guidelines

## Motivation

* Every developer should write unit and/or integration tests for a new code. By not writing
testable code and tests, the technical debt is created, which will be addressed later
on by code refactoring for tests. This habit should be avoided by writing testable code in the first place.
* Only by covering code with tests we ensure confidence for the future changes in the code without breaking other places.
* Writing the code and at the same time thinking about how to make it testable enforces us to stick to a good programming practices and principles like SOLID.

## Testable Code Characteristics
  * **Separation of concerns** - The key to writing testable code is to ensure that there is a strong separation between the different parts of an application, with clear, simple interfaces. Distinct areas of functionality like data retrieval, data processing, data display, and event handling should typically be separated into individual classes/modules/subsystems. Ensuring a separation between different parts of the code makes it possible for components to be easily replaced with mock objects during testing.
  * **Object-oriented, SOLID principles based design** - Using an object-oriented design with initialisers means that each test can instantiate a fresh copy of any object under test. This ensures that the potential for state being carried from one test to the next is minimised. It also makes it possible to use dependency injection to define mock dependencies during testing.
  * **Loose coupling / dependency injection** - Passing dependencies through initializers, instead of instantiating new instances or using global objects (e.g. singletons) directly makes code more testable.
  * **Clear code and documentation** - In order to write accurate tests, it is necessary that code is kept clean enough that test authors and future maintainers can quickly understand the purpose of each unit of code being tested and how it fits into the overall application.

## Test formatting

It’s good practice to format the test into **given**, **when** and **then** sections:

1. In the **given** section, set up any values needed
2. In the **when** section, execute the code being tested
3. In the **then** section, assert the result you expect

## F.I.R.S.T Principles of Unit Testing
* **Fast** - Setup, the actual test and tear down should execute really fast (milliseconds) as you may have thousands of tests in your entire project.

* **Independent/Isolated** - No order-of-run dependency. They should pass or fail the same way in suite or when run individually.

* **Repeatable** - Should yield the same results every time and at every location where they run. No dependency on date/time or random functions output.

* **Self-validating** - Tests should be fully automated; the output should be either “pass” or “fail”. No manual inspection required to check whether the test has passed or failed.

* **Timely** - Should cover every use case scenario and NOT just aim for 100% coverage. Should try to aim for Test Driven Development (TDD) so that code does not need re-factoring later.

## Testing Anti-patterns

* Create the world
  * Huge/complex setup in order to run a test.
  * Test complexity increases.
  * Often a sign of application complexity.
* The exceptional test
  * Not asserting anything.
  * Testing that exceptions are not thrown.
  * Good coverage, but poor validation.
* One big test
  * Testing multiple things.
  * Have lot of assertions.
  * Calling multiple methods and making assertions in between those calls.
  * When it does fail, it can be number of reasons.
* Second class test
  * Not clear what is the test purpose.
  * Test code that is poorly structured, unreadable.
  * Hard to maintain.
* Testing private methods
  * _It's worth mentioning that private methods shouldn't be exposed only for the testing purpose. If there is a need to test a private method, there is something conceptually wrong with that method. Usually it is doing too much to be a private method, which in turn violates the Single Responsibility Principle. Private methods should be tested by reaching them via the public methods._
* Singleton
  * Retains state over time by definition.
  * Difficult to trace usage in code.

## Reference

* [WWDC 2017 “Engineering for testability” session](https://developer.apple.com/videos/play/wwdc2017/414/)
* [Testable code best practices](https://www.sitepen.com/blog/2014/07/11/testable-code-best-practices/)
* [Raywenderlich.com iOS Unit Testing and UI Testing Tutorial](https://www.raywenderlich.com/150073/ios-unit-testing-and-ui-testing-tutorial)
* [F.I.R.S.T Principles of Unit Testing](https://github.com/ghsukumar/SFDC_Best_Practices/wiki/F.I.R.S.T-Principles-of-Unit-Testing)
* [Udemy course “Learn Test Driven Development in Java”](https://www.udemy.com/learn-test-driven-development-in-java/)
* [SOLID design principles in iOS](https://marcosantadev.com/solid-principles-applied-swift/)
* [Bad Testing Practices](https://www.objc.io/issues/15-testing/bad-testing-practices/)
