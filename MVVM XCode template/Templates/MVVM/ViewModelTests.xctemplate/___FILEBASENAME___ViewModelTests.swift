final class ___FILEBASENAME___: XCTestCase {
    private var viewModel: ___VARIABLE_fileName___ViewModel!
    private var interactor: ___VARIABLE_fileName___Interacting?
    private var coordinator: ___VARIABLE_fileName___CoordinatorSpy!
    
    override func setUp() {
        super.setUp()
        
        coordinator = ___VARIABLE_fileName___CoordinatorSpy()
        setupViewModel()
    }
    
    func testSomething() {}
}

extension ___FILEBASENAME___ {
    private func setupViewModel() {
        viewModel = ___VARIABLE_fileName___ViewModel()
        viewModel.coordinator = coordinator
        interactor = viewModel
    }
}

private class ___VARIABLE_fileName___CoordinatorSpy: ___VARIABLE_fileName___Coordinating {}
