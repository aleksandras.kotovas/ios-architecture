To install the Xcode templates, run the following command in this (Templates) directory:

> make install_templates

- After template is installed, it can be found in "MVVM" section, when creating new file.
- During creation, enter both capitalized and lowercased name of the new screen.

To uninstall Xcode templates, run:

> make uninstall_templates
