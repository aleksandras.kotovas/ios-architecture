protocol ___FILEBASENAME___ {
    func ___VARIABLE_lowercaseFileName___ViewController(coordinator: ___VARIABLE_fileName___Coordinating) -> ___VARIABLE_fileName___ViewController
}

extension ViewControllersFactory: ___FILEBASENAME___ {
    func ___VARIABLE_lowercaseFileName___ViewController(coordinator: ___VARIABLE_fileName___Coordinating) -> ___VARIABLE_fileName___ViewController {
        let viewModel = ___VARIABLE_fileName___ViewModel()
        viewModel.coordinator = coordinator
        let viewController = ___VARIABLE_fileName___ViewController(viewModel: viewModel)
        viewController.interactor = viewModel
        return viewController
    }
}
