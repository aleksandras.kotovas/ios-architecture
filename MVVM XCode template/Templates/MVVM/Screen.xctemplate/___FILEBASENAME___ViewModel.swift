protocol ___VARIABLE_fileName___Coordinating: AnyObject {}

final class ___FILEBASENAME___ {
    weak var coordinator: ___VARIABLE_fileName___Coordinating?
    
    // MARK: - Observables
    
    
    // MARK: - Initializer
    
    init() {}
}

extension ___FILEBASENAME___: ___VARIABLE_fileName___Interacting {
    func ___VARIABLE_lowercaseFileName___ExampleButtonPressed() {}
}
