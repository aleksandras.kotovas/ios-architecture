import UIKit

protocol ___VARIABLE_fileName___Interacting: AnyObject {
    func ___VARIABLE_lowercaseFileName___ExampleButtonPressed()
}

final class ___FILEBASENAME___: UIViewController {
    private let viewModel: ___VARIABLE_fileName___ViewModel
    weak var interactor: ___VARIABLE_fileName___Interacting?
    
    init(viewModel: ___VARIABLE_fileName___ViewModel) {
        self.viewModel = viewModel
        super.init(rvcConfiguration: configuration)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
    }
    
    func bindViewModel() {
    }
}
